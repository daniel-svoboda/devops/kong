FROM lwolf/helm-kubectl-docker:latest

COPY . /tmp/
WORKDIR /tmp

RUN kubectl config set-cluster apply-2-cluster --server="https://<SPECIFY_CLUSTER_IP>" --insecure-skip-tls-verify=true && \
	kubectl config set-credentials cluster_admin --username="<CLUSTER_USERNAME>" --password="<CLUSTER_USER_PWD>" && \
	kubectl config set-context default --cluster=apply-2-cluster --user=cluster_admin && \
	kubectl config use-context default



# to build image run from within terraform files directory:
	# docker build -t terraform_gce:v1 -f ../Dockerfile .
# or from where Dockerfile is:
# docker build -t terraform_infra:v1 .

# to run built image:
	# docker run -it --rm terraform_infra:v1 bash
# staging cluster


# then run container and do:
	# cd .. && helm install -n consul -f consul-helm/values.yaml ./consul-helm/
# if needed to delete:
	# helm del --purge consul
