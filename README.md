# Kong helm setup

# Setup:

### Ensure Helm is setup
[helm-setup](https://gitlab.com/daniel-svoboda/devops/helm-setup)
and for gcloud setup you may use [gcloud cli setup](https://gitlab.com/daniel-svoboda/devops/google-cloud-specific/gcloud-cli-setup)

### Create your Database + user
can be setup via Terraform: [Terraform apply](https://gitlab.com/daniel-svoboda/devops/terraform)

### Create kong namespace
`echo "
apiVersion: v1
kind: Namespace
metadata:
  name: kong" | kubectl create -f -`

### Create gcloud secrets
`kubectl create secret generic cloudsql-oauth-credentials \
--from-file=credentials.json=cred.json -n kong`
`kubectl create secret generic kong-secret \
--from-literal=KONG_DB_PASSWORD='SOME_SECRET' -n kong`

### Install Kong
`helm install \
	--set ingressController.enabled=true \
	-n kong \
	./kong`

### Create your custom kong-ingress setup and apply it
`kubectl create -f custom-kongingress.yaml`

### Change Kongs IP to `Static`
Manually in gcloud:
    - Gcloud > VPC Network > External IP Adresses > Change Kong's IP from `Ephemeral` to `Static`


# KONG API configuration endpoint:
`http http://awesometest.ga/routes Host:prod.admin.kong`

## Example of correct curl call to change Kong values:
`curl -i -X PATCH --url http://<KONG_IP>/routes/<ROUTE_ID/ --data strip_path=true -H "Host: staging.admin.kong"`


## Cleanup:
`helm delete --purge kong`

## Docs
- https://github.com/Kong/kubernetes-ingress-controller
- https://github.com/Kong/kubernetes-ingress-controller/blob/master/docs/annotations.md

## Ingress Annotations to use:
`annotations:
  kubernetes.io/ingress.class: "nginx"`

